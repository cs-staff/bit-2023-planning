/**
 * This contains the Markdown text for the homepage. 
 * 
 * It's done as a js file rather than raw md so that it will still work if loaded from a file:// url
 * (If index.html is loaded from file:// then Chrome will regard the origin as null and dom.fetch would
 * refuse to access .md file content. Instead, we load it as a script from a script tag)
 */

let text = `
# Bachelor of Information Technology planning

To edit the course structures, edit <code>courses.js</code> and hit reload in the browser.


`

setHomePage(text)